﻿class Book:
    def __init__(self, title):
        self._title = title

    def read(self):
        return 'Knowledge from {0}'.format(self._title)