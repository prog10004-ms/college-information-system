﻿from StudentModule import Student
from BusModule import Bus
from LibraryModule import Library
from FacultyModule import Faculty

#create a student object
student = Student('Larry', 100)

#TODO: repeating going to school until the user tells the program the term has ended

#send the student to school
bus = Bus()
student.goToSchool(bus)

#the student studies at school
schoolLibrary = Library()
student.study(schoolLibrary) #TODO: ask the user which book title should they borrow from the library to study

#ask the student to do homework
assignment = student.doHomework() #TODO: ask the user which assignment title or number to work on

#show the student's knowledge
student.showKnowledge()

#TODO: ask the user if the term has ended to check if the loop needs to finish

#create a faculty object and get the faculty to school
prof = Faculty('Magdin', 500, 'E202')
prof.goToSchool(bus)
