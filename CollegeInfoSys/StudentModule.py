﻿from KnowledgeModule import Knowledge
from CarModule import Car
from LaptopModule import Laptop
from AssignmentModule import Assignment
from UserModule import User

class Student(User): #Student IS-A User
    def __init__(self, name, id):
        #ensure the __init__ method of the base class is called to define all base attributes
        User.__init__(self, name, id)

        #define all the association relationships, the HAS-A relationships
        self._knowledge = Knowledge() #student HAS-A knowledge
   
    def doHomework(self):
        assign = Assignment('Assignment 1 by {0}'.format(self._name)) #Student USES Assignment
        self._knowledge.add("Knowledge from assignment '{0}'".format(assign.getTitle()))
        return assign

    def study(self, lib): #Student USES Library
        book = lib.borrow('Learning Python') #Students USES Library AND Student USES BOOK
        self._knowledge.add(book.read())  #Student USES Book

    def showKnowledge(self):
        self._knowledge.show()