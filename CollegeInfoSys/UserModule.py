from CarModule import Car
from LaptopModule import Laptop
class User:
    def __init__(self, name, id):
        self._name = name #user HAS-A name
        self._userId = id #user HAS-A user ID
        self._vehicle = Car()
        self._computer = Laptop()

    def goToSchool(self, bus): #Student USES Bus
        bus.drive() #Student USES Bus
